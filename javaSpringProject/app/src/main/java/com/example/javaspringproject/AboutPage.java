package com.example.javaspringproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class AboutPage extends AppCompatActivity {

    private Button mBack;
    private Intent intent;

     /*
    onCreate method to set up the buttons and to create the window for the activity.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_about_page);

        mBack = (Button)findViewById(R.id.back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainMenu();
            }
        });
    }


    //method to go back to the main menu from the about page
    public void openMainMenu(){
        intent = new Intent(this, MainMenu.class);
        //using intents to switch between the current activity and the respective.
        startActivity(intent);
    }
}