package com.example.javaspringproject;

//Other parts of the android libraries that we use
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class TheGame1 extends GameThread{

    //Will store the image of a ball
    private Bitmap mBall;

    //The X and Y position of the ball on the screen (middle of ball)
    private float mBallX = -10;
    private float mBallY = -10;

    //The speed (pixel/second) of the ball in direction X and Y
    private float mBallSpeedX = 0;
    private float mBallSpeedY = 0;

    //Defining the mechanics for the paddle of the
    private Bitmap mPaddle;
    private float mPaddleX = 0;
    private float mPaddleSpeedX = 0;

    //variable to work on the collision control
    private float mMinDistanceFromPaddle;

    //variables for the target that the user will be hitting to gain points
    private Bitmap mTarget;
    private float mTargetX = -100;
    private float mTargetY = -100;
    private float mTargetSpeed = 0;

    //In this particular level only one enemy, in TheGame2 and TheGame3, it is an array to store multiple enemies
    private Bitmap mSadBall;
    private float mSadBallX = -100;
    private float mSadBallY;

    //This is run before anything else, so we can prepare things here
    public TheGame1(GameView gameView) {
        //House keeping
        super(gameView);

        //Prepare the image so we can draw it on the screen (using a canvas)
        mBall = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.small_red_ball);

        mPaddle = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.yellow_ball);

        mTarget = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.smiley_ball);

        mSadBall = BitmapFactory.decodeResource
                (gameView.getContext().getResources(),
                        R.drawable.sad_ball);
    }

    //This is run before a new game (also after an old game)
    @Override
    public void setupBeginning() {
        //Initialise speeds
        mBallSpeedX = mCanvasWidth / 2;
        mBallSpeedY = mCanvasHeight / 2;
        mPaddleSpeedX = 0;
        mTargetSpeed = 0;

        //Place the ball in the middle of the screen.
        //mBall.Width() and mBall.getHeight() gives us the height and width of the image of the ball
        mBallX = mCanvasWidth / 2;
        mBallY = mCanvasHeight / 2;
        mPaddleX = mCanvasHeight / 2;
        mTargetX = mCanvasWidth / 2;
        mTargetY = mTarget.getHeight() / 2;

        mSadBallX = mCanvasWidth / 2;
        mSadBallY = mCanvasHeight/ 5;


        mMinDistanceFromPaddle = (mPaddle.getWidth()/2 + mBall.getWidth()/2) * (mPaddle.getWidth()/2 + mBall.getWidth()/2);
    }

    @Override
    protected void doDraw(Canvas canvas) {
        //If there isn't a canvas to draw on do nothing
        //It is ok not understanding what is happening here
        if(canvas == null) return;

        super.doDraw(canvas);

        //draw the image of the ball using the X and Y of the ball
        //drawBitmap uses top left corner as reference, we use middle of picture
        //null means that we will use the image without any extra features (called Paint)
        canvas.drawBitmap(mBall, mBallX - mBall.getWidth() / 2, mBallY - mBall.getHeight() / 2, null);
        canvas.drawBitmap(mPaddle, mPaddleX - mPaddle.getWidth() / 2, mCanvasHeight - mPaddle.getHeight() / 2,null);
        canvas.drawBitmap(mTarget, mTargetX - mTarget.getWidth() / 2, mTargetY - mTarget.getHeight() / 2, null);
        canvas.drawBitmap(mSadBall, mSadBallX - mSadBall.getWidth() / 2, mSadBallY - mSadBall.getHeight() / 2, null);

    }

    //This is run whenever the phone is touched by the user
    @Override
    protected void actionOnTouch(float x, float y) {
        mPaddleX = x;
    }


    /*
    This is run whenever the phone moves around its axises although not using it currently to
    instead use the touch screen sensor above
    */
    @Override
    protected void actionWhenPhoneMoved(float xDirection, float yDirection, float zDirection) {
//        mPaddleSpeedX = mPaddleSpeedX + 70f * xDirection;
//
//        if(mPaddleX <= 0 && mPaddleSpeedX < 0){
//            mPaddleSpeedX = 0;
//            mPaddleX = 0;
//        }
//        if(mPaddleX >= mCanvasWidth && mPaddleSpeedX > 0){
//            mPaddleSpeedX = 0;
//            mPaddleX = mCanvasWidth;
//        }

    }

    //This is run just before the game "scenario" is printed on the screen
    @Override
    protected void updateGame(float secondsElapsed) {
        float distanceBetweenBallAndTarget;

        if(mBallSpeedY > 0){
            updateCollision(mPaddleX, mCanvasHeight);
        }

        if(mBallSpeedY < 0){
            if(updateCollision(mTargetX, mTargetY)){
                updateScore(1);
            }
        }

        if(mBallSpeedY < 0){
            updateCollision(mSadBallX, mSadBallY);
        }

        //Move the ball's X and Y using the speed (pixel/sec)
        mBallX = mBallX + secondsElapsed * mBallSpeedX;
        mBallY = mBallY + secondsElapsed * mBallSpeedY;

        if((mBallX <= mBall.getWidth()/2 && mBallSpeedX < 0) || (mBallX >= mCanvasWidth - mBall.getWidth()/2 && mBallSpeedX > 0)){
            mBallSpeedX = -mBallSpeedX;
        }

        if((mBallY <= mBall.getHeight()/2 && mBallSpeedY < 0)){
            mBallSpeedY = -mBallSpeedY;
        }

        //Move the paddle's X and Y using the speed (pixel/sec)
        mPaddleX = mPaddleX + secondsElapsed * mPaddleSpeedX;
        mTargetX = mTargetX + secondsElapsed * mTargetSpeed;

        if(mBallY > mCanvasHeight){ //using this to stop the game if the user misses the ball with the paddle
            setState(STATE_LOSE);
        }
    }

    /*
    collision control method to make sure that when the white ball hits any of the other obstacles
    it is recorded and the correct action is taken by the game to change the direction of the white ball
     */
    public boolean updateCollision(float x, float y){
        float distanceBetweenBallAndPaddle = (x - mBallX) * (x - mBallX) + (y - mBallY) * (y - mBallY);

        if(mMinDistanceFromPaddle >= distanceBetweenBallAndPaddle){
            float speedOfBall = (float)Math.sqrt(mBallSpeedX * mBallSpeedX + mBallSpeedY * mBallSpeedY);

            mBallSpeedX = mBallX - x;
            mBallSpeedY = mBallY - y;

            float newSpeedOfBall = (float)Math.sqrt(mBallSpeedX * mBallSpeedX + mBallSpeedY * mBallSpeedY);

            mBallSpeedX = mBallSpeedX * speedOfBall / newSpeedOfBall;
            mBallSpeedY = mBallSpeedY * speedOfBall / newSpeedOfBall;

            return true;
        }
        return false;
    }
}
