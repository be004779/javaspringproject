package com.example.javaspringproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class MainMenu extends AppCompatActivity {

    private Button mAbout; //buttons to switch between activities
    private Button mPlay;
    private Intent intent;


    /*
    onCreate method to set up the buttons and to create the window for the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main_menu);

        mPlay = (Button)findViewById(R.id.play);
        mPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLevels();
            }
        });

        mAbout = (Button)findViewById(R.id.about);
        mAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAbout();
            }
        });
    }

    /*
    methods to open the other activities, in this case opening the levels page and the about page
     */

    public void openLevels(){
        intent = new Intent(this, LevelsPage.class);
        //using intents to switch between the current activity and the respective.
        startActivity(intent);
    }

    public void openAbout(){
        intent = new Intent(this, AboutPage.class);
        startActivity(intent);
    }
}