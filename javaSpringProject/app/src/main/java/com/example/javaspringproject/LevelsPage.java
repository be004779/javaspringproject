package com.example.javaspringproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class LevelsPage extends AppCompatActivity {

    private Button mLevel1;
    private Button mLevel2;
    private Button mLevel3;
    private Button mBack;
    private Intent intent;


     /*
    onCreate method to set up the buttons and to create the window for the activity.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_levels_page);

        mLevel1 = (Button)findViewById(R.id.level1);
        mLevel1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLevel1();
            }
        });

        mLevel2 = (Button)findViewById(R.id.level2);
        mLevel2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLevel2();
            }
        });

        mLevel3 = (Button)findViewById(R.id.level3);
        mLevel3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLevel3();
            }
        });

        mBack = (Button)findViewById(R.id.back);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainMenu();
            }
        });

    }

     /*
      methods to open the other activities, in this case opening the different levels for the game
     and being able to go back to the main menu page
     */

    public void openLevel1(){
        intent = new Intent(this, MainActivity1.class);
        //using intents to switch between the current activity and the respective.
        startActivity(intent);
    }

    public void openLevel2(){
        intent = new Intent(this, MainActivity2.class);
        startActivity(intent);
    }

    public void openLevel3(){
        intent = new Intent(this, MainActivity3.class);
        startActivity(intent);
    }

    public void openMainMenu(){
        intent = new Intent(this, MainMenu.class);
        startActivity(intent);
    }
}